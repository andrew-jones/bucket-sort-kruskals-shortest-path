/*  main.c - Andrew Jones 22/05/2014

    This code is designed for assignment 2 of ########. The purpose of this
    was to bucket sort a given set of data that represented a graph that
    was sorted in numerical order of from/to for each vertex. The bucket
    sort was to be used on edge weights to get them into numerical order
    to be used in a kruskal algorithm to find the shortest path.

    The assignment's runtime is O(E + V log V)
*/

#include <stdio.h> /* Used for printf and scanf */
#include <stdlib.h> /* Used for malloc, free and sizeof */

/*  Edge based linked lists
    Used for holding information about the graph

    first_vertex is the starting position of the edge
    second_vertex is the ending position of the edge
    weight is the value of the edge between vertices
    next is the location of the next item in linked list
 */
typedef struct edge_node *edge_node_ptr; 
struct edge_node 
{ 
    int first_vertex; 
    int second_vertex; 
    int weight; 
    edge_node_ptr next; 
};

/*  Standard linked lists 
    used for sets in kruskals algorithm

    data_item is the vertex number
    next is location of the next item in linked list
*/
typedef struct node *node_ptr;
struct node
{
    int data_item;
    node_ptr next;
};

/*  Read in user input and construct a linked list of input 
    
    @param edge_node_ptr start_node - An empty linked list
    @param int edges - Amount of edges user will input
*/
void read_graph(edge_node_ptr start_node, int edges);

/*  Sort a linked list of edges by their weight
    
    @param edge_node_ptr nodes - List of edges
*/
void bucket_sort(edge_node_ptr nodes);

/*  Perform kruskals shortest path algorithm on a linked list

    @param edge_node_ptr nodes - List of edges
    @param int vertices - Amount of vertices in graph
*/
void kruskal(edge_node_ptr nodes, int vertices);

/*  Outputs first and second vertex as well as weight of each
    node in a list.

    @param edge_node_ptr nodes - List of edges
*/
void output(edge_node_ptr nodes);

/*  Reads user input, bucket sorts it, runs kruskals shortest path,
    then outputs results before freeing it.

    @return Whether successfully terminated or not
*/
int main()
{
    int vertices;
    int edges;
    edge_node_ptr graph = (edge_node_ptr) malloc(sizeof(struct edge_node));
    edge_node_ptr temp;

    scanf("%d%d", &vertices, &edges);

    /* Due to constant times, this is still O(E + V log V) overall */
    read_graph(graph, edges); /* O(E), read in all edges */
    bucket_sort(graph); /* O(E), sort all edges */
    kruskal(graph, vertices); /* O(E + V log V) */
    output(graph); /* O(E), the remaining edges in list */

    /* free all remaining nodes */
    while(graph)
    {
        temp = graph;
        graph = graph->next;
        free(temp);
    }

    return EXIT_SUCCESS;
}

/*  Read in user input and construct a linked list of input 
    
    @param edge_node_ptr start_node - An empty linked list
    @param int edges - Amount of edges user will input
*/
void read_graph(edge_node_ptr start_node, int edges)
{
    int edges_read;
    int first;
    int second;
    int weight;
    edge_node_ptr previous;
    edge_node_ptr current;
    previous = start_node;

    /* read each edge and link up linked list */
    for(edges_read = 0; edges_read < edges; edges_read++)
    {
        current = (edge_node_ptr) malloc(sizeof(struct edge_node));
        current->next = NULL;

        scanf("%d%d%d", &first, &second, &weight);
        current->first_vertex = first;
        current->second_vertex = second;
        current->weight = weight;

        previous->next = current;
        previous = current;
    }
}

/*  Sort a linked list of edges by their weight
    
    @param edge_node_ptr nodes - List of edges
*/
void bucket_sort(edge_node_ptr nodes)
{
    edge_node_ptr buckets[10000];//hardcode required for assignment 
    edge_node_ptr bucket_rears[10000];//hardcode required for assignment 
    edge_node_ptr node = nodes->next;

    int i;
    int weight;
    int size = sizeof(buckets) / sizeof(buckets[i]);

    /* put all the nodes into relevant buckets */
    while(node)
    {
        weight = node->weight;
        if(buckets[weight])/*if something in bucket modify rear*/
        {
            bucket_rears[weight]->next = node;
            bucket_rears[weight] = node;
        }
        else /* otherwise put into new bucket */
        {
            buckets[weight] = (edge_node_ptr) malloc(sizeof(struct edge_node));
            buckets[weight]->next = node;
            bucket_rears[weight] = node;
        }

        node = node->next;/* get next node */
        bucket_rears[weight]->next = NULL;/* separate rear from node list */
    }
    /* connect all the buckets that contain something together, and free after */
    node = nodes;
    for(i = 0; i < size; i++)
    {
        if(bucket_rears[i]) /* If something in bucket */
        {
            node->next = buckets[i]->next;/* link to start of bucket */
            node = bucket_rears[i]; /* start from end of bucket */
            free(buckets[i]); /* Free all buckets */
        }
    }
}

/*  Perform kruskals shortest path algorithm on a linked list

    @param edge_node_ptr nodes - List of edges
    @param int vertices - Amount of vertices in graph
*/
void kruskal(edge_node_ptr nodes, int vertices)
{
    int v; /* temporary vertex */
    int find_first; /* location of first vertex in set */
    int find_second; /* location of second vertex in set */
    int find_array[100000];//hardcode required for assignment 
    node_ptr sets[100000];//hardcode required for assignment 
    node_ptr temp; /* used to temporarily hold node_ptrs during moving */
    node_ptr temp_temp; /* used to temporarily hold temp whilst freeing temp at end */
    edge_node_ptr solution = (edge_node_ptr) malloc(sizeof(struct edge_node)); /* list of 'good' values */
    edge_node_ptr try_node = nodes->next; /* iterates over given nodes */
    edge_node_ptr solution_end = solution; /* end of 'good' values */
    edge_node_ptr temp_node; /* temporarily holds the try_node while changing */

    /* initialise sets and 'buckets' for find_array */
    for(v = 0; v < vertices; v++)
    {
        temp = (node_ptr) malloc(sizeof(struct node));
        temp->data_item = v;
        temp->next = NULL;

        find_array[v] = v;
        sets[v] = (node_ptr) malloc(sizeof(struct node));
        sets[v]->data_item = 1;
        sets[v]->next = temp;
    }

    /* Run the kruskal algorithm on each node in the list */
    while(try_node)
    {
        find_first = find_array[try_node->first_vertex]; /* Get location of first vertex */
        find_second = find_array[try_node->second_vertex]; /* Get location of second vertex */
        temp_node = try_node;
        try_node = try_node->next;

        /* if first and second not in same set, then union sets */
        if(find_first != find_second)
        {
            /* add to the good list - this wont work if end result doesnt have a single tree */
            solution_end->next = temp_node;
            solution_end = temp_node;
            solution_end->next = NULL; /* dont want this pointing to rest of list */

            /* if more items in first, first becomes second, second becomes first */
            if(sets[find_first]->data_item > sets[find_second]->data_item) 
            {
                v = find_first;
                find_first = find_second;
                find_second = v;
            }
            /* update size of both */
            sets[find_second]->data_item += sets[find_first]->data_item;
            sets[find_first]->data_item = 0;
            /* update set location of each item in first set*/
            temp = sets[find_first];
            while(temp->next)
            {
                temp = temp->next;
                v = temp->data_item; /* get the vertex */
                find_array[v] = find_second; /* change location to second */
            }
            /* move first to second, temp at correct place for next part due to while loop */
            temp->next = sets[find_second]->next; /* end of first connects to second */  
            sets[find_second]->next = sets[find_first]->next; /* first then comes 'in front' of second */
            sets[find_first]->next = NULL; /* nothing left in first */
        }
        else
        {
             free(temp_node); /* This node wont be needed again */
        }
    }

    nodes->next = solution->next; /* nodes are set to the 'good' list */

    /* free all sets and lists within */
    for(v = 0; v < vertices; v++)
    {
        temp = sets[v]->next;
        while(temp) /* free anything in lists */
        {
            temp_temp = temp->next;
            free(temp);
            temp = temp_temp;
        }
        free(sets[v]); /* free the set */
    }

    free(solution);
}

/*  Outputs first and second vertex as well as weight of each
    node in a list.

    @param edge_node_ptr nodes - List of edges
*/
void output(edge_node_ptr nodes)
{
    /* Loop through entire list */
    while(nodes->next)
    {
        nodes = nodes->next;
        printf("%d %d %d\n", nodes->first_vertex, nodes->second_vertex, nodes->weight);
    }
}


